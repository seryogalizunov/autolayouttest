//
//  AppDelegate.h
//  AutolayoutTest
//
//  Created by Lizunov on 11/30/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

